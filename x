[1mdiff --git a/Controllers/EvaluadorController.cs b/Controllers/EvaluadorController.cs[m
[1mindex bb3bcc8..5e4eb60 100644[m
[1m--- a/Controllers/EvaluadorController.cs[m
[1m+++ b/Controllers/EvaluadorController.cs[m
[36m@@ -1357,7 +1357,7 @@[m [mnamespace WsPFApps.Controllers[m
 [m
 [m
         [HttpGet][m
[31m-        public object RevisaHistorialInterno(int Evaluacion, int Prospecto, string RFC = "")[m
[32m+[m[32m        public object RevisaHistorialInterno(int Evaluacion, int Prospecto, int FolioTitular=0, string RFC = "")[m
         {[m
             try[m
             {[m
[36m@@ -1369,6 +1369,7 @@[m [mnamespace WsPFApps.Controllers[m
                 CONN.SetCommand("MotorEvaluador.SP_MotorEvaluacion_RevisaHistorialInterno");[m
                 CONN.CreateParameter("@Id_Evaluacion", Evaluacion);[m
                 CONN.CreateParameter("@Id_Prospecto", Prospecto);[m
[32m+[m[32m                CONN.CreateParameter("@FolioTitular", FolioTitular);[m
                 CONN.CreateParameter("@RFC", RFC);[m
 [m
                 HistorialSolicitudes = CONN.getDataSet();[m
[36m@@ -1417,29 +1418,39 @@[m [mnamespace WsPFApps.Controllers[m
         {[m
             try[m
             {[m
[31m-                int Exito = 1;[m
[31m-                float PagoCalculado = 0, Tasa = 0;[m
[31m-                string Err_Mensage = "", PagoCalculadoStr = "";[m
[32m+[m[32m                int Exito = 1, PlazoMaximo = 0;[m
[32m+[m[32m                float PagoCalculado = 0, Tasa = 0, Comision=0, MontoComision= 0;[m
[32m+[m[32m                string Err_Mensage = "", PagoCalculadoStr = "", ComisionStr = "0", MontoComisionStr = "0", TipoPlazo = "", Garantia= "";[m
                 [m
                 DataTable Pago;[m
 [m
                 CONN.SetCommand("Evaluador.SP_CalculaPagoFijo");                [m
                 CONN.CreateParameter("@Evaluacion_Id", Evaluacion);[m
[32m+[m[32m                CONN.CreateParameter("@Prospecto_Id", 0); //Si envío 0, toma el Tipo Titular por defualt[m
[32m+[m[32m                CONN.CreateParameter("@Producto", Producto);[m
                 CONN.CreateParameter("@Monto", Monto);[m
                 CONN.CreateParameter("@PagosTotales", Plazo);[m
                 CONN.CreateParameter("@Frecuencia", Frecuencia);[m
                 CONN.CreateParameter("@PagoFijo", 0);[m
[31m-[m
[32m+[m[41m                               [m
                 CONN.ExecuteNonQuery();[m
                 Pago = CONN.getDataTable();[m
                [m
                 PagoCalculadoStr = Convert.ToString(Pago.Rows[0]["Pago"]);[m
[31m-                PagoCalculado   = float.Parse(PagoCalculadoStr);[m
[31m-                Tasa            = Convert.ToInt32(Pago.Rows[0]["Tasa"]);[m
[31m-                Exito           = Convert.ToInt32(Pago.Rows[0]["Exito"]);[m
[31m-                Err_Mensage     = Convert.ToString(Pago.Rows[0]["Err_Mensaje"]);[m
[31m-                                [m
[31m-                return new { Exito = Exito, Err_Mensaje = Err_Mensage, Pago = PagoCalculado, Tasa = Tasa};[m
[32m+[m[32m                PagoCalculado       = float.Parse(PagoCalculadoStr);[m
[32m+[m[32m                Tasa                = Convert.ToInt32(Pago.Rows[0]["Tasa"]);[m
[32m+[m[32m                Exito               = Convert.ToInt32(Pago.Rows[0]["Exito"]);[m
[32m+[m[32m                Err_Mensage         = Convert.ToString(Pago.Rows[0]["Err_Mensaje"]);[m
[32m+[m[32m                PlazoMaximo         = Convert.ToInt32(Pago.Rows[0]["PlazoMax"]);[m
[32m+[m[32m                Garantia            = Convert.ToString(Pago.Rows[0]["Garantia"]);[m
[32m+[m[32m                ComisionStr         = Convert.ToString(Pago.Rows[0]["Comision"]);[m
[32m+[m[32m                Comision            = float.Parse(ComisionStr);[m
[32m+[m[32m                MontoComisionStr    = Convert.ToString(Pago.Rows[0]["MontoComision"]);[m[41m                [m
[32m+[m[32m                MontoComision       = float.Parse(MontoComisionStr);[m
[32m+[m[32m                TipoPlazo           = Convert.ToString(Pago.Rows[0]["TipoPlazo"]);[m
[32m+[m[32m                Frecuencia          = Convert.ToInt32(Pago.Rows[0]["Frecuencia"]);[m
[32m+[m
[32m+[m[32m                return new { Exito = Exito, Err_Mensaje = Err_Mensage, Pago = PagoCalculado, Tasa = Tasa, PlazoMaximo, Garantia, Comision, MontoComision, TipoPlazo, Frecuencia };[m
             }[m
             catch (Exception ex)[m
             {[m
[1mdiff --git a/Web.config b/Web.config[m
[1mindex 531a545..5d13f12 100644[m
[1m--- a/Web.config[m
[1m+++ b/Web.config[m
[36m@@ -12,8 +12,8 @@[m
   <connectionStrings>    [m
     <!--<add name="BacklogDB" connectionString="Initial Catalog=BASEWS;Data Source=172.16.81.200;User ID=usrPFeliz;password=.pfeliz+;Pooling=False;Connect Timeout=3600;" providerName="System.Data.SqlClient" />-->[m
     <!--add name="BacklogDB" connectionString="Server=DESKTOP-8674M2V;UID=sa;PWD=root;Database=wsBase;  Connection Timeout=3600;" providerName="System.Data.SqlClient" /-->[m
[31m-    <!--<add name="Evaluador" connectionString="Initial Catalog=Evaluador;Data Source=172.16.81.200;User ID=JuanOrtega;password=abcd1234;Pooling=False;Connect Timeout=3600;" providerName="System.Data.SqlClient" />-->[m
[31m-    <add name="Evaluador" connectionString="Initial Catalog=Evaluador_0413;Data Source=DESKTOP-8674M2V;User ID=sa;password=root;Pooling=False;Connect Timeout=3600;" providerName="System.Data.SqlClient" />[m
[32m+[m[32m    <add name="Evaluador" connectionString="Initial Catalog=Evaluador;Data Source=172.16.81.200;User ID=JuanOrtega;password=abcd1234;Pooling=False;Connect Timeout=3600;" providerName="System.Data.SqlClient" />[m
[32m+[m[32m    <!--add name="Evaluador" connectionString="Initial Catalog=Evaluador_0413;Data Source=DESKTOP-8674M2V;User ID=sa;password=root;Pooling=False;Connect Timeout=3600;" providerName="System.Data.SqlClient" /-->[m
   </connectionStrings>[m
   <appSettings>[m
     <!--add key="CADENACONEXIONSQL" value="BacklogDB" /-->[m
[36m@@ -21,7 +21,7 @@[m
     <add key="auth_token_test" value="test" />[m
     <add key="idApp" value="1" />[m
     <add key="esquema" value="dbo" />[m
[31m-    <add key="tiempo_sesion" value="720" />[m
[32m+[m[32m    <add key="tiempo_sesion" value="5" />[m
     [m
       <add key="cors:Origins" value="http://localhost" />[m
       <add key="cors:OriginsApp" value="*" />[m
