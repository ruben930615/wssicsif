﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class Movil
    {
        public int Id_Usuario { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Ubicacion { get; set; }
        public string Bateria { get; set; }
        public int Id_RegistroMovil { get; set; }
    }
}