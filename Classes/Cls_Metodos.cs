﻿using System;
using System.Linq;
using System.Security.Claims;

namespace WsPFApps.Classes
{
    public class Cls_Metodos
    {
        public string ObtieneValorDesdeToken(string Tipo,ClaimsPrincipal principal)
        {   
            string valor = principal.Claims.Where(c => c.Type == Tipo).Single().Value;
            return valor;
        }

        public string RandomString(int length)
        {
            System.Random r = new System.Random();
            const string pool = "abcdefghijklmnopqrstuvwxyz0123456789";
            var chars = Enumerable.Range(0, length).Select(x => pool[r.Next(0, pool.Length)]);
            return new string(chars.ToArray());
        }

        public string[][] Split(string[] arrayIn, int length)
        {
            bool even = arrayIn.Length % length == 0;
            int totalLength = arrayIn.Length / length;
            if (!even)
                totalLength++;

            string[][] newArray = new string[totalLength][];
            for (int i = 0; i < totalLength; ++i)
            {
                int allocLength = length;
                if (!even && i == totalLength - 1)
                    allocLength = arrayIn.Length % length;

                newArray[i] = new string[allocLength];
                Array.Copy(arrayIn, i * length, newArray[i], 0, allocLength);
            }
            return newArray;
        }
    }
}