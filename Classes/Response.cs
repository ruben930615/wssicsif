﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class Response
    {
        public int Exito { get; set; }
        public string Err_Mensaje { get; set; }
        public int Numero { get; set; }
    }
}