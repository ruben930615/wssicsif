﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{

    public class address_components
    {
        public string Long_name { get; set; }
        public string Short_name { get; set; }
    }

    public class viewport
    {
        public Location Northeast { get; set; }
        public Location Southwest { get; set; }
    }

    public class geometry
    {
        public Location Location { get; set; }
        public string Location_type { get; set; }
        public viewport Viewport { get; set; }
    }

    public class results
    {
        public List<address_components> Address_components { get; set; }
        public string Formatted_address { get; set; }
        public geometry Geometry { get; set; }
        public string Place_id { get; set; }
    }

    public class GoogleGeolocationObj
    {
        public List<results> Results { get; set; }
        public string Status { get; set; }
        public string Error_message { get; set; }
    }
}