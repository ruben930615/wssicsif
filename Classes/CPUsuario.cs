﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class CPUsuario
    {
        public int Id_Usuario { get; set; }
        public int Id_UsuarioAlta { get; set; }
        public int Id_UsuarioBaja { get; set; }
        public List<string> ListaCP= new List<string>();
    }
}