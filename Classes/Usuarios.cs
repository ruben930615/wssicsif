﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class Usuarios
    {
        public int Id_Usuario{ get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string DenominacionSocial { get; set; }
        public string Rfc { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Tipo { get; set; }
        public string Correo { get; set; }
        public string NombreUsuario { get; set; }
        public string Clave { get; set; }
        public string Usu { get; set; }
        public string Pwd { get; set; }
        public string PwdConfirma { get; set; }
        public string PwdNuevo { get; set;  }
        public string PwdNuevoConfirma { get; set; }
        public int Origen { get; set; }
        public int Id_Sucursal { get; set; }
        public int Id_Area { get; set; }
        public int Id_Rol { get; set; }

    }
}