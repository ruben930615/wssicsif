﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class CuestionarioNegocio
    {

        public int Id_Visita { get; set; }
        public int Id_TipoVisita { get; set; }
        public string NombreAtiende { get; set; }
        public string Parentesco { get; set; }
        public string TipoIdentificacion { get; set; }
        public string FolioIdentificacion { get; set; }
        public string DescripcioActividad { get; set; }
        public int Id_TipoLocal { get; set; }
        public int AcreditaPropiedad { get; set; }
        public string MedidasLocal { get; set; }
        public int Id_DescripcionLocal { get; set; }
        public string DiaDeLaboral { get; set; }
        public string DiaALaboral { get; set; }
        public string HoraInicioLaboral { get; set; }
        public string HoraFinLaboral { get; set; }
        public int NumeroEmpleados { get; set; }
        public int Id_Competencia { get; set; }
        public int Id_Afluencia { get; set; }
        public string DescripcionMobiliario { get; set; }
        public int MontoInversionMobiliario { get; set; }
        public string DescripcionMercancia { get; set; }
        public int MontoInversionMercancia { get; set; }
        public int MontoInversionMensual { get; set; }
        public int IngresosMensual { get; set; }
        public int GastosMensuales { get; set; }
        public int PagoRentaLocal { get; set; }
        public int UtilidadMensual { get; set; }
        public byte[] Foto { get; set; }
        public string Observaciones { get; set; }
        public int Id_ResultadoInvestigacion { get; set; }
        public string Ubicacion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public int Estatus { get; set; }
        public DateTime FechaCuestionario { get; set; }
    }
}