﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class Location
    {
        public float Lat { get; set; }
        public float Lng { get; set; }
    }
}