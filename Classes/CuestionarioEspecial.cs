﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class CuestionarioEspecial
    {
        public int Id_Visita { get; set; }
        public int Id_TipoVisita { get; set; }
        public string NombreAtiende { get; set; }
        public string Parentesco { get; set; }
        public string TipoIdentificacion { get; set; }
        public string FolioIdentificacion { get; set; }
        public byte[] Foto { get; set; }
        public string Observaciones { get; set; }
        public int Id_ResultadoInvestigacion { get; set; }
        public string Ubicacion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public int Estatus { get; set; }
        public DateTime FechaCuestionario { get; set; }
    }
}