﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class CuestionarioDomicilio
    {

        public int Id_Visita { get; set; }
        public int Id_TipoVisita { get; set; }
        public string NombreAtiende { get; set; }
        public string TipoIdentificacion { get; set; }
        public string FolioIdentificacion { get; set; }
        public int Id_TipoInmueble { get; set; }
        public string AniosResidencia { get; set; }
        public string MesesResidencia { get; set; }
        public int Id_TipoVivienda { get; set; }
        public int Id_TipoColonia { get; set; }
        public int NumeroHabitaciones { get; set; }
        public string MaterialPiso { get; set; }
        public string DescripcionVivienda { get; set; }
        public string IncongruenciaVivienda { get; set; }
        public int Dependientes { get; set; }
        public int MontoPago { get; set; }
        public int AutomovilesPropios { get; set; }
        public int ValorEstimadoAutomoviles { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public string EntreCalles { get; set; }
        public string AniosResidenciaValidacion { get; set; }
        public string MesesResidenciaValidacion { get; set; }
        public byte[] Foto { get; set; }
        public string Observaciones { get; set; }
        public int Id_ResultadoInvestigacion { get; set; }
        public string Ubicacion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public int Estatus { get; set; }
        public DateTime FechaCuestionario { get; set; }
    }
}