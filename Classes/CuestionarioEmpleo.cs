﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class CuestionarioEmpleo
    {
        public int Id_Visita { get; set; }
        public int Id_TipoVisita { get; set; }
        public string NombreAtiende { get; set; }
        public string PuestoAtiende { get; set; }
        public string TipoIdentificacion { get; set; }
        public string FolioIdentificacion { get; set; }
        public int Id_TipoTrabajo { get; set; }
        public string PuestoCliente { get; set; }
        public int Antiguedad { get; set; }
        public int SueldoMensual { get; set; }
        public int ActualmenteLaborando { get; set; }
        public string DiaDeLaboral { get; set; }
        public string DiaALaboral { get; set; }
        public string HoraInicioLaboral { get; set; }
        public string HoraFinLaboral { get; set; }
        public string Departamento { get; set; }
        public byte[] Foto { get; set; }
        public string Observaciones { get; set; }
        public int Id_ResultadoInvestigacion { get; set; }
        public string Ubicacion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public int Estatus { get; set; }
        public DateTime FechaCuestionario { get; set; }

    }
}