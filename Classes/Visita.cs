﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Classes
{
    public class Visita
    {
        public int Numero { get; set; }
        public int Id_Visita { get; set; }
        public int Id_Usuario { get; set; }
        public string Usuario { get; set; }
        public string Folio { get; set; }
        public string Id_Cliente { get; set; }
        public string NombreCliente { get; set; }
        public int Id_TipoVisita { get; set; }
        public string TipoVisita { get; set; }
        public string CP { get; set; }
        public string Colonia { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string Calle { get; set; }
        public string NumInterior { get; set; }
        public string NumExterior { get; set; }
        public string EntreCalles { get; set; }
        public string ReferenciaDomicilio { get; set; }
        public string TipoInmueble { get; set; }
        public string AniosResidencia { get; set; }
        public string MesesResidencia { get; set; }
        public string TelefonoCelular { get; set; }
        public string TelefonoOficina { get; set; }
        public string NombreEmpresa { get; set; }
        public string JefeInmediato { get; set; }
        public string Departamento { get; set; }
        public string Puesto { get; set; }
        public string Giro { get; set; }
        public string TipoLocal { get; set; }
        public string DescripcionLocal { get; set; }
        public string DescripcionActividad { get; set; }
        public string NombreArrendador { get; set; }
        public string TelefonoArrendador { get; set; }
        public string Especificaciones { get; set; }
        public DateTime FechaVisitaCreada { get; set; }
        public DateTime FechaInicioVisitaCreada { get; set; }
        public DateTime FechaFinVisitaCreada { get; set; }
        public DateTime FechaVisitaConcluida { get; set; }
        public DateTime FechaInicioVisitaConcluida { get; set; }
        public DateTime FechaFinVisitaConcluida { get; set; }
        public int Estatus { get; set; }
        public int CargaMasiva { get; set; }
        public string ContenidoBase64 { get; set; }

    }
}