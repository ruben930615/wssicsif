/****** Object:  Table [dbo].[TB_CatUsuarioApps]    Script Date: 30/10/2017 01:00:26 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_CatUsuarioApps](
	[idUsuarioApp] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idApp] [int] NOT NULL,
	[token] [varchar](8) NULL DEFAULT '',
	[privateKey] [varchar](2048) NULL DEFAULT '',
	[ultimaActividad] [datetime] NULL,
	[ip] [varchar](30) NOT NULL DEFAULT '',
	[numeroPeticion] [int] NOT NULL DEFAULT 0,
	[idEstatus] [int] NOT NULL DEFAULT 1,
 CONSTRAINT [PK_TB_CatUsuarioApps] PRIMARY KEY CLUSTERED 
(
	[idUsuarioApp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

