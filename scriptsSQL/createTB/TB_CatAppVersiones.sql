/****** Object:  Table [dbo].[TB_CatAppVersiones]    Script Date: 30/10/2017 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_CatAppVersiones](
	[idAppVersion] [int] IDENTITY(1,1) NOT NULL,
	[idApp] [int] NOT NULL,
	[idPlataforma] [int] NOT NULL,
	[version] [float] NOT NULL,
	[fechaLanzamiento] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_TB_AppVersiones] PRIMARY KEY CLUSTERED 
(
	[idAppVersion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-iOS
2-Android' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TB_CatAppVersiones', @level2type=N'COLUMN',@level2name=N'idPlataforma'
GO


