/****** Object:  Table [dbo].[TB_CATUsuario]    Script Date: 30/10/2017 11:02:54 a.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_CatUsuarios](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellidoP] [varchar](50) NULL,
	[apellidoM] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[usuario] [varchar](30) NULL,
	[clave] [varbinary](256) NULL,
	[idEstatus] [int] NOT NULL,
CONSTRAINT [PK_TB_CatUsuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TB_CatUsuarios] ADD  CONSTRAINT [DF_TB_CatUsuarios_IdEstatus]  DEFAULT ((1)) FOR [IdEstatus]
GO

