/****** Object:  Table [dbo].[TB_CatApps]    Script Date: 30/10/2017 12:59:25 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_CatApps](
	[idApp] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](60) NOT NULL,
	[tiempoSesion] [int] NOT NULL,
 CONSTRAINT [PK_TB_CatApps] PRIMARY KEY CLUSTERED 
(
	[idApp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TB_CatApps] ADD  DEFAULT ((0)) FOR [tiempoSesion]
GO


