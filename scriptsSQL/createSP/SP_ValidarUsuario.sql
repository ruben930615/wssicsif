SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		JP
-- Create date: 2017/11/03
-- Description:	SP utilizado para validar usuario
-- =============================================
CREATE PROCEDURE [dbo].[SP_ValidarUsuario] 
	-- Add the parameters for the stored procedure here
	@usuario varchar(120),
	@clave varchar(120),
	@token varchar(10),
	@idApp int = null,
	@privateKey varchar(2048)
AS
--EXEC dbo.SP_ValidarUsuario @usuario = 'demo', @clave = 'demo', @token = '123', @privateKey = '123', @idApp = 1
BEGIN

	DECLARE @valido int = 0,
			@idUsuario int
	
	SET @usuario = upper(@usuario)

	SELECT 
		@valido = 1,
		@idUsuario = IdUsuario
	FROM
		TB_CatUsuarios  
    WHERE 
		upper(usuario) = @usuario  AND 
        PWDCOMPARE (UPPER(@clave), clave) = 1 AND
        idEstatus = 1

	IF @valido = 1
	BEGIN
		IF @idApp IS NOT NULL
		BEGIN
			SET @valido = 0

			SELECT 
				@valido = 1 
			FROM 
				TB_CatUsuarioApps 
			WHERE 
				idApp = @idApp AND 
				idUsuario = @idUsuario

			IF @valido = 1
			BEGIN
				UPDATE 
					TB_CatUsuarioApps
				SET
					token = @token,
					privateKey = @privateKey
				WHERE 
					idApp = @idApp AND 
					idUsuario = @idUsuario
			END
		END
	END

	IF @valido = 1
	BEGIN
		IF @idApp IS NOT NULL
		BEGIN
			SELECT 1 as 'valido', u.idUsuario, u.nombre, u.apellidoP, u.apellidoM, u.email, u.usuario, ua.token 
			FROM 
				TB_CatUsuarios u 
				INNER JOIN TB_CatUsuarioApps ua ON u.idUsuario = ua.idUsuario AND ua.idApp = @idApp
			WHERE 
				ua.idApp = @idApp AND
				ua.idUsuario = @idUsuario AND
				ua.idEstatus = 1

			UPDATE TB_CatUsuarioApps SET numeroPeticion = 0 WHERE idUsuario = @idUsuario AND idApp = @idApp AND idEstatus = 1
		END
		ELSE
		BEGIN
			SELECT 
				1 as 'valido', u.idUsuario, u.nombre, u.apellidoP, u.apellidoM, u.email, u.usuario
			FROM 
				TB_CatUsuarios u 
			WHERE 
				u.usuario = @usuario
		END
	END
	ELSE
	BEGIN
		SELECT 0 as 'valido'
	END
END
GO


