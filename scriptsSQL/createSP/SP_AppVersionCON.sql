SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		JP
-- Create date: 2017/08/21
-- Description:	SP para consulta version reciente de aplicacion
-- =============================================
CREATE PROCEDURE [SP_AppVersionCON] 
	-- Add the parameters for the stored procedure here
	@idApp int,
	@idPlataforma int -- 1-iOS/2-Android
AS
BEGIN
	SELECT 
		top 1 app.idApp, nombre, idPlataforma, version, fechaLanzamiento 
	FROM 
		TB_CatAppVersiones v 
		INNER JOIN TB_CatApps app ON v.idApp = app.idApp 
	WHERE 
		app.idApp = @idApp AND idPlataforma = @idPlataforma ORDER BY version DESC;
END
GO
