SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		JP
-- Create date: 2017/11/06
-- Description:	SP utilizado para obtener llave privada de usuario
-- =============================================
CREATE PROCEDURE [dbo].[SP_ObtenerLlavePrimariaUsuario] 
	-- Add the parameters for the stored procedure here
	@idUsuario int,
	@idApp int
AS
--EXEC dbo.SP_ObtenerLlavePrimariaUsuario @idUsuario = 1, @idApp = 1
BEGIN

	SELECT 
		privateKey
	FROM
		TB_CatUsuarioApps  
    WHERE 
		idUsuario = @idUsuario AND
		idApp = @idApp AND
		idEstatus = 1
END
GO


