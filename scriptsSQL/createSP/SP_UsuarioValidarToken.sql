SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		JP
-- Create date: 2017/11/06
-- Description:	SP utilizado para validar token y numero de peticiones
-- =============================================
CREATE PROCEDURE [dbo].[SP_UsuarioValidarToken] 
	-- Add the parameters for the stored procedure here
	@idUsuario int,
	@idApp int,
	@token varchar(10),
	@numeroPeticion int
AS
--EXEC dbo.SP_UsuarioValidarToken 1, 1, 'tylp', 2
BEGIN
	DECLARE @valido int = 0
	DECLARE @mensaje varchar(100) = 'Token invalido'

	SELECT 
		@valido = 1,
		@mensaje = 'Token valido'
	FROM
		TB_CatUsuarioApps  
    WHERE 
		idEstatus = 1 AND
		idUsuario = @idUsuario AND
		idApp = @idApp AND
		token = @token AND
		(@numeroPeticion = 0 OR numeroPeticion + 1 = @numeroPeticion)

	--En caso de estar enviando el numero de peticion, entonces aumentamos en 1
	IF @valido = 1 AND @numeroPeticion > 0
	BEGIN
		UPDATE 
			TB_CatUsuarioApps 
		SET 
			numeroPeticion = numeroPeticion + 1
		WHERE 
			idEstatus = 1 AND
			idUsuario = @idUsuario AND
			idApp = @idApp
	END

	--Siempre actualizamos la ultima actividad del usuario
	UPDATE 
		TB_CatUsuarioApps 
	SET
		ultimaActividad = GETDATE() 
	WHERE 
		idEstatus = 1 AND
		idUsuario = @idUsuario AND
		idApp = @idApp

	SELECT @valido as 'valido', @mensaje as 'mensaje'
END
GO


