﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
// using System.Web.Mvc;
using System.Data.SqlClient;
using WsPFApps.Models;
using WsPFApps.Classes;
using System.Data;
using CalculaRFCPisa;
using CalculaCURPPisa;
using System.IO;
using WsPFApps.Models.DAO;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.Configuration;
using System.Runtime.Serialization.Formatters.Binary;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WsPFApps.Controllers
{

    [RoutePrefix("api/SICMAS")]
    public class SICMASController : ApiController
    {
        private Cls_BD.Cls_Coneccion CONN = new Cls_BD.Cls_Coneccion();




        #region Visitas


        [HttpPost]
        public async Task<Response> AltaManualVisita(Visita v)
        {
            try
            {
                int Exito;
                string Err_Mensage;

                Exito = 1;
                Err_Mensage = "Visita agregada correctamente.";
                var response = await GeolocationGoogle(v.Calle, v.NumExterior, v.Colonia, v.Municipio, v.Estado, "Mexico");

                if (response.Results.Count() == 0 || !(response.Status.Equals("OK")))
                {
                    Exito = 0;
                    Err_Mensage = "No se pudo encontrar la dirección indicada. Favor de intentar mas tarde.";

                    return new Response { Exito = Exito, Err_Mensaje = Err_Mensage };
                }

                var latitud = response.Results[0].Geometry.Location.Lat;
                var longitud = response.Results[0].Geometry.Location.Lng;

                CONN.SetCommand("SIC.SP_Tb_VisitasALT");
                CONN.CreateParameter("@Latitud", latitud);
                CONN.CreateParameter("@Longitud", longitud);

                if (v.Id_TipoVisita == Convert.ToInt32(Tipo.COBRANZA))
                {
                    CONN.CreateParameter("@Cliente_Id", v.Id_Cliente);
                    CONN.CreateParameter("@NombreCliente", v.NombreCliente);
                    CONN.CreateParameter("@Folio", v.Folio);
                    CONN.CreateParameter("@TipoVisita_Id", v.Id_TipoVisita);
                    CONN.CreateParameter("@Usuario", v.Usuario);
                    CONN.CreateParameter("@Calle", v.Calle);
                    CONN.CreateParameter("@Colonia", v.Colonia);
                    CONN.CreateParameter("@Municipio", v.Municipio);
                    CONN.CreateParameter("@Estado", v.Estado);
                    CONN.CreateParameter("@CP", v.CP);
                    CONN.CreateParameter("@NumExterior", v.NumExterior);
                    CONN.CreateParameter("@NumInterior", v.NumInterior);
                    CONN.CreateParameter("@Especificaciones", v.Especificaciones);
                    CONN.CreateParameter("@CargaMasiva", v.CargaMasiva);

                }

                if (v.Id_TipoVisita == Convert.ToInt32(Tipo.ESPECIAL))
                {
                    CONN.CreateParameter("@Cliente_Id", v.Id_Cliente);
                    CONN.CreateParameter("@NombreCliente", v.NombreCliente);
                    CONN.CreateParameter("@Folio", v.Folio);
                    CONN.CreateParameter("@TipoVisita_Id", v.Id_TipoVisita);
                    CONN.CreateParameter("@Calle", v.Calle);
                    CONN.CreateParameter("@Colonia", v.Colonia);
                    CONN.CreateParameter("@Municipio", v.Municipio);
                    CONN.CreateParameter("@Estado", v.Estado);
                    CONN.CreateParameter("@CP", v.CP);
                    CONN.CreateParameter("@NumExterior", v.NumExterior);
                    CONN.CreateParameter("@NumInterior", v.NumInterior);
                    CONN.CreateParameter("@TelefonoCelular", v.TelefonoCelular);
                    CONN.CreateParameter("@TelefonoOficina", v.TelefonoOficina);
                    CONN.CreateParameter("@Especificaciones", v.Especificaciones);
                    CONN.CreateParameter("@CargaMasiva", v.CargaMasiva);

                }


                if (v.Id_TipoVisita == Convert.ToInt32(Tipo.DOMICILIO)
                    || v.Id_TipoVisita == Convert.ToInt32(Tipo.LABORAL)
                    || v.Id_TipoVisita == Convert.ToInt32(Tipo.NEGOCIO))
                {

                    CONN.CreateParameter("@Cliente_Id", v.Id_Cliente);
                    CONN.CreateParameter("@NombreCliente", v.NombreCliente);
                    CONN.CreateParameter("@Folio", v.Folio);
                    CONN.CreateParameter("@TipoVisita_Id", v.Id_TipoVisita);
                    CONN.CreateParameter("@CP", v.CP);
                    CONN.CreateParameter("@Colonia", v.Colonia);
                    CONN.CreateParameter("@Municipio", v.Municipio);
                    CONN.CreateParameter("@Estado", v.Estado);
                    CONN.CreateParameter("@Calle", v.Calle);
                    CONN.CreateParameter("@NumExterior", v.NumExterior);
                    CONN.CreateParameter("@NumInterior", v.NumInterior);
                    CONN.CreateParameter("@EntreCalles", v.EntreCalles);
                    CONN.CreateParameter("@ReferenciaDomicilio", v.ReferenciaDomicilio);
                    CONN.CreateParameter("@TipoInmueble", v.TipoInmueble);
                    CONN.CreateParameter("@AniosResidencia", v.AniosResidencia);
                    CONN.CreateParameter("@MesesResidencia", v.MesesResidencia);
                    CONN.CreateParameter("@TelefonoCelular", v.TelefonoCelular);
                    CONN.CreateParameter("@TelefonoOficina", v.TelefonoOficina);
                    CONN.CreateParameter("@Especificaciones", v.Especificaciones);



                    if (v.Id_TipoVisita == Convert.ToInt32(Tipo.LABORAL))
                    {
                        CONN.CreateParameter("@NombreEmpresa", v.NombreEmpresa);
                        CONN.CreateParameter("@JefeInmediato", v.JefeInmediato);
                        CONN.CreateParameter("@Departamento", v.Departamento);
                        CONN.CreateParameter("@Puesto", v.Puesto);

                    }

                    if (v.Id_TipoVisita == Convert.ToInt32(Tipo.NEGOCIO))
                    {
                        CONN.CreateParameter("@Giro", v.Giro);
                        CONN.CreateParameter("@TipoLocal", v.TipoLocal);
                        CONN.CreateParameter("@DescripcionLocal", v.DescripcionLocal);
                        CONN.CreateParameter("@DescripcionActividad", v.DescripcionActividad);
                        CONN.CreateParameter("@NombreArrendador", v.NombreArrendador);
                        CONN.CreateParameter("@TelefonoArrendador", v.TelefonoArrendador);


                    }

                }

                DataSet visita;
                visita = CONN.getDataSet();

                Exito = Convert.ToInt32(visita.Tables[0].Rows[0][0]);
                Err_Mensage = Convert.ToString(visita.Tables[0].Rows[0][1]);

                return new Response { Exito = Exito, Err_Mensaje = Err_Mensage };
            }
            catch (Exception ex)
            {
                int Exito = 0;
                string Err_Mensage = ex.Message.ToString();
                return new Response { Exito = Exito, Err_Mensaje = Err_Mensage };
            }
        }

        [HttpGet]
        public async Task<GoogleGeolocationObj> GeolocationGoogle(string calle, string numeroExterior, string colonia, string ciudad, string estado, string pais)
        {

            string apiKey = "AIzaSyA69zEC2QW7RrJjVFB_NfJbsMgCBR-p-t0";
            string address = string.Format("{0} {1}, {2}, {3}, {4}, {5}", calle, numeroExterior, colonia, ciudad, estado, pais);
            string url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + apiKey;

            var request = WebRequest.Create(url);

            WebResponse response = request.GetResponse();
            var webResponse = await Task<WebResponse>.Factory.FromAsync
            (request.BeginGetResponse(null, null),
            request.EndGetResponse);


            using (var reader2 = new StreamReader
            (webResponse.GetResponseStream()))
            {
                string json2 = reader2.ReadToEnd();
                var result2 = JsonConvert.DeserializeObject<GoogleGeolocationObj>(json2);
                return result2;
            }
        }



        #endregion












    }
}
