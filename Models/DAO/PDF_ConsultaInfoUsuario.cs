﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.AcroForms;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace WsPFApps.Models.DAO
{
    public class PDF_ConsultaInfoUsuario
    {
        public int Id_Usuario { set; get; }
        public string NombreUsuario { set; get; }
        public string Clave { set; get; }
        public string Rfc { get; set; }


        public PdfDocument pdf;
        PdfPage page;
        XGraphics graph;
        XFont font;
        public PdfDocument newDoc;

        public MemoryStream getMemoryStream()
        {
            MemoryStream stream = new MemoryStream();
            newDoc.Save(stream, false);
            //newDoc.Save(@WebConfigurationManager.AppSettings["UploadsFolder"] + "test.pdf");
            return stream;
        }

        void EscribirLinea(string campo, string texto)
        {
            PdfTextField pdfNombre = (PdfTextField)(pdf.AcroForm.Fields[campo]);
            pdfNombre.Value = new PdfString(texto);
            pdfNombre.ReadOnly = true;
        }

        
        public string CrearPDF_ConsultaInfoUsuario()
        {
            try
            {

                //pdf = PdfReader.Open(@WebConfigurationManager.AppSettings["pdf_consultaburo"], PdfDocumentOpenMode.Import);
                pdf = PdfReader.Open("C:\\inetpub\\wwwroot\\wsVirtualizadorMAS\\img\\FormatoConsultaInfoUsuario.pdf", PdfDocumentOpenMode.Import);
                //pdf = PdfReader.Open("C:\\Users\\Ruben.Bautista\\Documents\\Proyectos\\wsvirtualizador\\img\\FormatoConsultaInfoUsuario.pdf", PdfDocumentOpenMode.Import);



                newDoc = new PdfDocument();
                if (pdf.AcroForm.Elements.ContainsKey("/NeedAppearances") == false)
                    pdf.AcroForm.Elements.Add("/NeedAppearances", new PdfBoolean(true));
                else pdf.AcroForm.Elements["/NeedAppearances"] = new PdfBoolean(true);

                EscribirLinea("IdUsuario", this.Id_Usuario.ToString());
                EscribirLinea("NombreUsuario", this.NombreUsuario);
                EscribirLinea("Clave", this.Clave);
                EscribirLinea("Fecha", DateTime.Now.ToShortDateString());

                page = newDoc.AddPage(pdf.Pages[0]);
                graph = XGraphics.FromPdfPage(page);
                font = new XFont("Arial", 11, XFontStyle.Regular);

                return "ok";
            }
            catch (Exception e)
            {

                return e.Message.ToString();
            }

        }
    }
}