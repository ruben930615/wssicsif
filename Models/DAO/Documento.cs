﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsPFApps.Models.DAO
{
    public class Documento
    {
        public int Prospecto { get; set; }
        public int TipoDocumento { get; set; }
        public string FotoBase64 { get; set; }
        public string MIMEType { get; set; }
    }
}