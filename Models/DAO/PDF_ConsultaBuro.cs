﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Diagnostics;
using System.IO;
using System.Web.Configuration;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.AcroForms;

namespace WsPFApps.Models.DAO
{
    public class PDF_ConsultaBuro
    {
        public string FechaLugar { set; get; }
        public string NombreSolicitante { set; get; }
        public string RFC { set; get; }
        public string CalleNumero { set; get; }
        public string Colonia { set; get; }
        public string Municipio { set; get; }
        public string Estado { set; get; }
        public string CP { set; get; }
        public string Telefono { set; get; }
        public string FechaConsultaBuro { set; get; }

        public PdfDocument pdf;
        PdfPage page;
        XGraphics graph;
        XFont font;
        public PdfDocument newDoc;


        public PDF_ConsultaBuro(){}

        public MemoryStream getMemoryStream()
        {
            MemoryStream stream = new MemoryStream();
            newDoc.Save(stream, false);
            newDoc.Save(@WebConfigurationManager.AppSettings["UploadsFolder"] + "test.pdf");
            return stream;
        }

        void EscribirLinea(string campo, string texto)
        {
            PdfTextField pdfNombre = (PdfTextField)(pdf.AcroForm.Fields[campo]);
            pdfNombre.Value = new PdfString(texto);
            pdfNombre.ReadOnly = true;
        }

        public void CrearPDF_ConsultaBuro()
        {
            pdf = PdfReader.Open(@WebConfigurationManager.AppSettings["pdf_consultaburo"], PdfDocumentOpenMode.Import);
            newDoc = new PdfDocument();
            if (pdf.AcroForm.Elements.ContainsKey("/NeedAppearances") == false)
                pdf.AcroForm.Elements.Add("/NeedAppearances", new PdfBoolean(true));
            else pdf.AcroForm.Elements["/NeedAppearances"] = new PdfBoolean(true);

            EscribirLinea("FechaLugar", this.FechaLugar);

            EscribirLinea("NombreSolicitante", this.NombreSolicitante);
            EscribirLinea("RFC", this.RFC);
            EscribirLinea("CalleNumero", this.CalleNumero);
            EscribirLinea("Colonia", this.Colonia);
            EscribirLinea("Municipio", this.Municipio);
            EscribirLinea("Estado", this.Estado);
            EscribirLinea("CP", this.CP);
            EscribirLinea("Telefono", this.Telefono);

            EscribirLinea("FechaConsultaBuro", this.FechaConsultaBuro);

            page = newDoc.AddPage(pdf.Pages[0]);
            graph = XGraphics.FromPdfPage(page);
            font = new XFont("Arial", 11, XFontStyle.Regular);
        }

        public void ConsultaProductos()
        {
            string fileName = @"c://inetpub//wwwroot//wsEvaluador//plantillas//Plandetrabajo.pdf";
            byte[] pdfByteArray = System.IO.File.ReadAllBytes(fileName);
            string base64EncodedPDF = System.Convert.ToBase64String(pdfByteArray);            
        }

    }
}