﻿using System.ComponentModel.DataAnnotations;

namespace WsPFApps.Models
{
    public class Usuario
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
        public string email { get; set; }
        public string usuario { get; set; }
        public string nombre_completo { get { return this.nombre + " " + this.apellido_paterno + " " + this.apellido_materno; } }
        public string token { get; set; }
        public int exito { get; set; }
        public string mensaje { get; set; }

        public Usuario()
        {
            this.id = 0;
            this.nombre = "";
            this.apellido_paterno = "";
            this.apellido_materno = "";
            this.email = "";
            this.usuario = "";
            this.token = "";
            this.exito = 0;
            this.mensaje = "";
        }
    }
}